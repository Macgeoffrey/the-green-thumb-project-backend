#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

import cherrypy, os, json, datetime
import app.dblib.db_interact as di
from app.adminlib.admin import error_page
from app.adminlib.auth_checks import check_session, check_user, check_token


#Static HTML pages returned to browser.
class basicSite(object):
    
    @cherrypy.expose
    def index(self):
        with open("static/index.htm") as h:
            index = h.read()
            return index

    @cherrypy.expose
    def script(self):
        with open("static/gtp.js") as s:
            script = s.read()
            return script


#Function(s) to allow public access to greenhouse data from main page.
#/api/
@cherrypy.expose
class api(object):

    #This is the API for the user's browser to GET sensor readings from a
    #starting time, and for the sensors to POST readings.  The POST requires
    #the sensor to send the correct token.
    #Retrieve sensor readings
    @cherrypy.tools.accept(media='application/json')
    def GET(self, time):

        try:
            assert ((datetime.datetime.utcnow().timestamp() * 1000) > int(time) > -1)
        except:
            raise cherrypy.HTTPError(400)

        try:
            return repr(di.fetch_timeframe(time))
        except:
            raise cherrypy.HTTPError(404)

    #Sensor send information to webserver
    @cherrypy.tools.accept(media='application/json')
    def POST(self):
        
        try:
            json_body = json.loads(cherrypy.request.body.read())
        except:
            print("invalid JSON in API POST")
            raise cherrypy.HTTPError(400)
        
        try:
            check_token(json_body['sensor_id'], json_body['token'])
            di.create_reading(json_body)
            return "Reading added"
        except:
            print("Something is configured wrong on sensor #" +
                    str(json_body['sensor_id']))
            raise cherrypy.HTTPError(401)


#URL to authenticate user
#/auth/
@cherrypy.expose
class auth(object):
    
    #Accept login credentials from user, then check the credentials
    #against the database.  If successful the user is given a cookie.
    @cherrypy.tools.accept(media='application/json')
    def POST(self):
        #Uncomment to troubleshoot session issues
        #cherrypy.session.debug=True
        try:
            json_body = json.loads(cherrypy.request.body.read())
        except:
            print("invalid JSON in auth")
            raise cherrypy.HTTPError(400)
        
        if check_user(json_body):
            cherrypy.session.load()
            return "/admin/"
        else:
            raise cherrypy.HTTPError(401)


#URL to logout user
#/logout
@cherrypy.expose
class logout(object):

    #When users reaches URN their session will be deleted
    @cherrypy.tools.accept(media='application/json')
    def GET(self):
        #Uncomment to troubleshoot session issues
        #cherrypy.session.debug=True
        try:
            cherrypy.session.delete()
        except:
            raise cherrypy.HTTPError(401)
        
        raise cherrypy.HTTPRedirect("/")


#Admin functions to modify all greenhouse data.  Authentication required.
#/admin/
@cherrypy.expose
class admin(object):
    
    #Retrieve admin page, script, and any database item(s)
    @cherrypy.tools.accept(media='application/json')
    def GET(self, item="admin.htm"):
        check_session(self)
        if item == "admin.htm":
            with open("static/admin.htm") as ah:
                adminSite = ah.read()
                return adminSite
        elif item == "admin.js":
            with open("static/admin.js") as aj:
                adminScript = aj.read()
                return adminScript
        else:
            try:
                return repr(di.fetch_all(item))
            except:
                raise cherrypy.HTTPError(404)

    #Create new item
    @cherrypy.tools.accept(media='application/json')
    def POST(self, item):
        check_session(self)
        try:
            json_body = json.loads(cherrypy.request.body.read())
        except:
            print("invalid JSON in admin POST")
            raise cherrypy.HTTPError(400)

        if item == 'greenhouses':
            di.create_greenhouse(json_body)
            return "Greenhouse added\n"
        elif item == 'sensors':
            di.create_sensor(json_body)
            return "Sensor added\n"
        elif item == 'users':
            di.create_user(json_body)
            return "User added\n"
        else:
            raise cherrypy.HTTPError(404)

    #Change information on item
    @cherrypy.tools.accept(media='application/json')
    def PATCH(self, item, item_id):
        check_session(self)
        try:
            json_body = json.loads(cherrypy.request.body.read())
        except:
            print("invalid JSON in admin PATCH")
            raise cherrypy.HTTPError(400)

        try:
            di.update_item(item, item_id, json_body)
            return item + " " + item_id + " has been updated\n"
        except:
            raise cherrypy.HTTPError(404)

    #Delete item
    @cherrypy.tools.accept(media='application/json')
    def DELETE(self, item, item_id):
        check_session(self)
        try:
            di.delete_item(item, item_id)
            return item + " " + item_id + " has been removed\n"
        except:
            raise cherrypy.HTTPError(404)


#Server configuration
cherrypy.config.update({
    'server.socket_host': '0.0.0.0',
    'server.socket_port': 443,
    'server.thread_pool': 1,
    'server.ssl_certificate': "ssl/gtp.pem",
    'server.ssl_private_key': "ssl/gtp-key.pem",
    'error_page.default': error_page,
    'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
    'tools.sessions.storage_path': os.path.join(os.path.dirname(os.path.abspath(__file__)), "sessions"),
    'tools.sessions.timeout': 60,
    })

if __name__ == '__main__':
    http_conf = {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__)),
            },
        '/admin': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.sessions.secure': True,
            'tools.sessions.httponly': True
            },
        '/api': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
            },
        '/auth': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.sessions.secure': True,
            'tools.sessions.httponly': True
            },
        '/logout': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './static'
            }
        }


    webapp = basicSite()
    webapp.admin = admin()
    webapp.api = api()
    webapp.auth = auth()
    webapp.logout = logout()
    cherrypy.quickstart(webapp, '/', http_conf)

