#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

if __name__ == "__main__":
    import sys
    sys.path.insert(0, '/usr/local/gtp')


import datetime, sys, os, sqlalchemy
from sqlalchemy import create_engine, ForeignKey, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from app.adminlib.admin import token_generator, pass2hash


### Create SQLite db file for greenhouse and sensor information,
### and enable ORM option
engine = create_engine('sqlite:///' + os.path.join(os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__)))), "gtp.db"))
Base = declarative_base()


### Greenhouse table
class Greenhouse(Base):
    __tablename__ = 'greenhouses'

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)

    sensors = relationship("Sensor", cascade="all, delete-orphan")

    ### Return Greenhouse objects as JSON formatted string
    def __repr__(self):
        return f'{{ "id" : "{self.id}", "name" : "{self.name}" }}' 

### Sensor table
class Sensor(Base):
    __tablename__ = 'sensors'

    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)
    token = Column(String(20))
    greenhouse_id = Column(Integer, ForeignKey('greenhouses.id'), nullable=False)
    greenhouse = relationship("Greenhouse", backref="greenhouses", single_parent=True, cascade="all, delete-orphan")

    readings = relationship("Reading", cascade="all, delete-orphan")

    ### Add new Sensor to database
    def __init__(self, name, greenhouse_id):
        self.name = name
        self.token = token_generator()
        self.greenhouse_id = greenhouse_id

    ### Return Sensor objects as JSON formatted string
    def __repr__(self):
        return f'{{ "id" : "{self.id}", "name" : "{self.name}", "token" : "{self.token}", "greenhouse_id" : "{self.greenhouse_id}", "greenhouse_name" : "{self.greenhouse.name}" }}'


### Table holding sensor readings
class Reading(Base):
    __tablename__ = 'readings'

    id = Column(Integer, primary_key=True)
    time = Column(Integer, nullable=False)
    sensor_id = Column(Integer, ForeignKey('sensors.id'), nullable=False)
    sensor = relationship("Sensor", backref="sensors", single_parent=True, cascade="all, delete-orphan")
    reading = Column(String(20))

    ### Add new sensor reading to database
    def __init__(self, sensor_id, reading):
        self.time = int(datetime.datetime.utcnow().timestamp() * 1000)
        self.sensor_id = sensor_id
        self.reading = reading
    
    ### Return Readings as JSON formatted string
    def __repr__(self):
        return f'{{ "time" : "{self.time}", "sensor" : "{self.sensor.name}", "reading" : "{self.reading}" }}'

#Table holding admin users
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(100), nullable=False, unique=True)
    password = Column(String(100), nullable=False)

    ### Add new user to database
    def __init__(self, username, password):
        self.username = username
        self.password = pass2hash(password)

    ### Return User as JSON formatted string
    def __repr__(self):
        return f'{{ "id" : "{self.id}", "username" : "{self.username}" }}'

### Build the tables
Base.metadata.create_all(engine)
