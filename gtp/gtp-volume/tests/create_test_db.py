#Copyright 2018, 2019 Joshua White
#This file is part of The Greenthumb Project Backend Server.

#The Greenthumb Project Backend Server is free software: you can redistribute 
#it and/or modify it under the terms of the GNU General Public License as 
#published by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#The Greenthumb Project Backend Server is distributed in the hope that it will
#be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
#Public License for more details.

#You should have received a copy of the GNU General Public License
#along with The Greenthumb Project Backend Server.  If not,
#see <https://www.gnu.org/licenses/>.

if __name__ == "__main__":
    import sys
    sys.path.insert(0, '/usr/local/gtp')

import app.dblib.db_setup as ds
import app.dblib.db_interact as di
import json, os, datetime

### This is a quick way to make brand new db for manual testing and deletion.
### This test is for dev only and shouldn't run on an existing db.

### Create temp database
print("\n")
print("Creating test database")
ds.Base.metadata.create_all(ds.engine)

### Create all temp items
print("\n")
try:
    print("Creating test Greenhouse")
    json_greenhouse = json.loads('{"name": "test"}')
    di.create_greenhouse(json_greenhouse)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making a Greenhouse"

try:
    print("Creating test Sensor")
    json_sensor = json.loads('{"name": "test", "greenhouse_id": "1"}')
    di.create_sensor(json_sensor)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making Sensor"

try:
    print("Creating test Reading")
    json_reading = json.loads('{"sensor_id": "1", "reading": "test"}')
    di.create_reading(json_reading)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making Reading"

try:
    print("Creating test User")
    json_user = json.loads('{"username": "test", "password": "test"}')
    di.create_user(json_user)
    print("Done")
except:
    os.remove(os.path.join(os.path.dirname(os.path.dirname(
              os.path.abspath(__file__))), "gtp.db"))
    raise "Problem making User"
