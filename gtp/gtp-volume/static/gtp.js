//Copyright 2018, 2019 Joshua White
//This file is part of The Greenthumb Project Backend Server.
//
//The Greenthumb Project Backend Server is free software: you can redistribute 
//it and/or modify it under the terms of the GNU General Public License as 
//published by the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//The Greenthumb Project Backend Server is distributed in the hope that it will
//be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
//Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with The Greenthumb Project Backend Server.  If not,
//see <https://www.gnu.org/licenses/>.

Vue.component('show-readings', {
  props: ['readings'],
  methods: {
    timeConvert: function (ms) {
      var d = new Date(+ms);
      return d.toLocaleString();
    }
  },
  template: `
    <table>
      <tr>
        <th>Time</th>
        <th>Sensor Name</th>
        <th>Reading</th>
      </tr>
      <tr v-for="(reading, index) in readings">
        <td
          v-if="!(index % 2)" 
          class="w3-center">{{ timeConvert(reading.time) }}</td>
        <td 
          v-if="!(index % 2)" 
          class="w3-center">{{ reading.sensor }}</td>
        <td 
          v-if="!(index % 2)" 
          class="w3-center">{{ reading.reading }}</td>
        <td 
          v-if="(index % 2)" 
          class="w3-center w3-gray">{{ timeConvert(reading.time) }}</td>
        <td 
          v-if="(index % 2)" 
          class="w3-center w3-gray">{{ reading.sensor }}</td>
        <td 
          v-if="(index % 2)" 
          class="w3-center w3-gray">{{ reading.reading }}</td>
      </tr>
    </table>
  `
})

var app = new Vue({
  el: '#app',
  data: {
    downloading: 'False',
    readings: null,
    attempt: ''
  },
  mounted: function () {
    this.getReadings();
  },
  methods: {
    getReadings: function (time) {
      this.downloading = 'True';
      if (typeof(time) === 'undefined') {
        var time = Date.now();
        time -= (60 * 60 * 24 * 7 * 1000);
        time -= (new Date().getTimezoneOffset() * 60000);
      }
      window.axios
        .get('/api/' + time)
        .then(response => (this.readings = response.data.reverse(), 
          this.downloading = 'False'))
        .catch(error => (this.readings = "Couldn't get readings."))
    },
    getTime: function (ms) {
      if (ms === 'all') {
        this.getReadings(0);
      } else {
        var time = new Date;
        time -= (ms);
        time -= (new Date().getTimezoneOffset() * 60000);
        this.getReadings(time);
      }
    },
    sendLogin: function () {
      window.axios
        .post('/auth/', {"username": this.username, "password": this.password})
        .then(response => (window.location = response.data))
        .catch(error => (this.attempt = "Try again."))
    }
  }
})

function ButtonPress(e) {
  if(e.keyCode === 13) {
	e.preventDefault();
	document.getElementById("submitBtn").click();
  }
}

function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

Morris.Line({
  element: 'myfirstchart',
  data: [
    { y: '1013', a: 68.33, b: 90 },
    { y: '1019', a: 72.61,  b: 65 },
    { y: '1021', a: 72.61,  b: 40 },
    { y: '1024', a: 72.61,  b: 65 },
    { y: '1027', a: 87.46,  b: 40 },
    { y: '1030', a: 89.49,  b: 65 },
    { y: '1033', a: 88.53, b: 90 }
  ],
  xkey: 'y',
  //ykeys: ['a', 'b'],
  ykeys: ['a'],
  labels: ['Water Temperature', 'Series B'],
    resize: true,
    lineColors: ['#74c043'],
});